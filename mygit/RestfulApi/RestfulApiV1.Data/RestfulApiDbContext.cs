﻿using System;
using Microsoft.EntityFrameworkCore;
using RestfulApiV1.Data.Entity;

namespace RestfulApiV1.Data
{
    public class RestfulApiDbContext:DbContext
    {
        public RestfulApiDbContext()
        {
        }

        public RestfulApiDbContext(DbContextOptions options):base(options)
        {
            
        }



        public DbSet<Users> Users { get; set; }

        public DbSet<Roles> Roles { get; set; }

        public DbSet<UserRole> UserRoles { get; set; }

        public DbSet<RolePermission> RolePermissions { get; set; }
    }
}
