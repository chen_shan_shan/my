﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace RestfulApiV1.Data.Shared
{
    public class PermissionDto
    {
        public string Title { get; set; }
        public string Name { get; set; }
    }
}
