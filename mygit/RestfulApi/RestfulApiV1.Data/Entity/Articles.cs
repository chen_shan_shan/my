﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RestfulApiV1.Data.Entity
{
    public class Articles
    {
        public bool IsActived;
        public bool IsDeleted;
        public int ArticlesId;
        public string PermissionName;
        public object Id;

        public int ArticleId { get; set; }
        public string ArticleName { get; set; }
        public string SubHeading { get; set; }
        public string ShorContent { get; set; }

        public string Author { get; set; }

        public string Description { get; set; }
        public string Remarks { get; set; }
    }
}
