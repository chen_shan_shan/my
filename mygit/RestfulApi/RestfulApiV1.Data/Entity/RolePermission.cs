﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RestfulApiV1.Data.Entity
{
    public class RolePermission:BaseEntity
    {
        public int RolesId { get; set; }
        public string PermissionName { get; set; }

        public virtual Roles Roles { get; set; }
    }
}
