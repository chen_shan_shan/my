﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RestfulApiV1.Interface.Paged
{
    public interface IPagination<T>
    {
        int PageIndex { get; }
        int PageSize { get; }
        int TotalItem { get; }

        int TotalPage { get; }

        IEnumerable<T> List { get; }
    }
}
