﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RestfulApiV1.Domain.DTOs
{
    public class UpdateUserRoleDto
    {
        public int[] RoleIds { get; set; }
    }
}
