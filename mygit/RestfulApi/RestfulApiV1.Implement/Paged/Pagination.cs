﻿using RestfulApiV1.Interface.Paged;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RestfulApiV1.Implement.Paged
{
    public class Pagination<T> : IPagination<T>
    {
        private readonly int _pageIndex;
        private readonly int _pageSize;
        private readonly int _totalItem;
        private readonly int _totalPage;
        private readonly IEnumerable<T> _list;

        public Pagination(IQueryable<T> source,int pageIndex,int pageSize)
        {
            _list = new List<T>();
            _pageIndex = pageIndex;
            _pageSize = pageSize;
            _totalItem = source.Count();
            _totalPage = _totalItem / _pageSize;

            _totalPage = _totalItem % _pageSize > 0 ? _totalPage + 1 : _totalPage;


            _list = source.Skip((_pageIndex - 1) * _pageSize).Take(_pageSize).ToList();
        }


        public int PageIndex { get { return _pageIndex; }}
        public int PageSize { get { return _pageSize; }  }
        public int TotalItem { get { return _totalItem; } }
        public int TotalPage { get { return _totalPage; }  }
        public IEnumerable<T> List { get { return _list; } }

        
    }

    public static class PaginationExtension
    {
        //public Pagination<dynamic> ConvertDynamic(this Pagination<T> source)
        //{

        //}
    }
}
