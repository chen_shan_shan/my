﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using RestfulApiV1.Data.Entity;
using RestfulApiV1.Data.Shared;
using RestfulApiV1.Interface.Data;
using RestfulApiV1.Utils.Json;

namespace RestfulApiV1.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ArticlesController : ControllerBase
    {
        private readonly IRespository<Articles> _articlesRespository;
        private readonly IRespository<Articles> _articlePermissionRespository;

        public ArticlesController(IRespository<Articles> articlesRespository, IRespository<Articles> articlePermissionRespository)
        {
            _articlesRespository = articlesRespository;
            _articlePermissionRespository = articlePermissionRespository;
        }

        [HttpPut("{id}")]
        public string Put(int id, Articles article)
        {
            var item = _articlesRespository.GetById(id);
            dynamic res;
            if (item != null)
            {
                item.ArticleName = article.ArticleName;
                item.Description = article.Description;
                _articlesRespository.Update(item);
                res = new { code = 1000, data = GetDynamic(item), msg = "修改文章成功" };
                return JsonHelper.SerializeObject(res);
            }
            else
            {
                res = new { code = 1000, data = "", msg = "当前文章不存在" };
            }

            return JsonHelper.SerializeObject(res);
        }

        [HttpDelete("{id}")]
        public string Delete(int id)
        {
            var item = _articlesRespository.GetById(id);
            dynamic res;
            if (item != null)
            {
                item.IsDeleted = true;
                _articlesRespository.Update(item);
                res = new { code = 1000, data = "", msg = "删除文章成功" };
            }
            else
            {
                res = new { code = 1000, data = "", msg = "当前文章不存在" };
            }

            return JsonHelper.SerializeObject(res);
        }

        [HttpPost, Route("{id}/permission")]
        public string SaveArticles(int id, [FromBody] IEnumerable<PermissionDto> reqList)
        {
            var list = _articlePermissionRespository.Table.Where(x => x.ArticlesId == id).ToList();
            _articlePermissionRespository.Delete(list);

            var articlePermissionList = new List<Articles>();
            foreach (var item in reqList)
            {
                articlePermissionList.Add(new Articles
                {
                    ArticlesId = id,
                    PermissionName = item.Name
                });
            }

            _articlePermissionRespository.Add(articlePermissionList);

            var article = _articlesRespository.GetById(id);

            var resData = new
            {
                article.Id,
                article.ArticleName,
                article.Description,
                article.Remarks,
                access = reqList.Select(x => x.Name).ToList()
            };


            var res = new { code = 1000, data = resData, msg = "文章权限保存成功" };
            return JsonHelper.SerializeObject(res);
        }

        private dynamic GetDynamic(Articles article)
        {
            var articlePermission = _articlePermissionRespository.Table
              //  .Where(x => x.ArticlesId == article.Id)
                .Select(x => x.PermissionName)
                .ToList();

            return new
            {
                article.Id,
                article.ArticleName,
                article.Description,
                article.Remarks,
                Access = articlePermission
            };
        }

        private dynamic GetDynamic(IEnumerable<Articles> articles)
        {
            var articlePermission = _articlePermissionRespository.Table
                .Where(x => articles.Select(t => t.Id).Contains(x.ArticlesId))
                .Select(x => new { x.Id, x.ArticlesId, x.PermissionName })
                .ToList();

            var resData = new List<dynamic>();
            foreach (var article in articles)
            {
                resData.Add(new
                {
                    article.Id,
                    article.ArticleName,
                    article.Description,
                    article.Remarks,
                   // Access = articlePermission.Where(x => x.ArticlesId == article.Id).Select(x => x.PermissionName).ToList()
                });
            }

            return resData;
        }
    }
}
