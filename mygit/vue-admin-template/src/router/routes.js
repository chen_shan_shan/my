/* 布局 */
import Layout from '@/layout'

// 路由定义（即所有可能的路由，定义在此）
const routes = [
  {
    path: '/login',
    name: 'login',
    component: () => import('@/views/login/index'),
    hidden: true
  },
  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [{
      path: 'dashboard',
      name: 'Dashboard',
      component: () => import('@/views/dashboard/index'),
      meta: { title: 'Dashboard', icon: 'dashboard' }
    }]
  },

  {
    path: '/example',
    component: Layout,
    redirect: '/example/table',
    name: 'Example',
    meta: { title: 'Example', icon: 'el-icon-s-help' },
    children: [
      {
        path: 'table',
        name: 'Table',
        component: () => import('@/views/table/index'),
        meta: { title: 'Table', icon: 'table' }
      },
      {
        path: 'tree',
        name: 'Tree',
        component: () => import('@/views/tree/index'),
        meta: { title: 'Tree', icon: 'tree' }
      }
    ]
  },

  {
    path: '/shopping',
    component: Layout,
    redirect: '/shopping/product',
    name: 'shopping',
    meta: { title: '商城', icon: 'el-icon-s-help', access: ['shopping'] },
    children: [
      {
        path: 'product',
        name: 'product',
        component: () => import('@/views/shopping/product/index'),
        meta: { title: '商品', icon: 'table', access: ['product'] }
      },
      {
        path: 'tree01',
        name: 'Tree01',
        component: () => import('@/views/tree/index'),
        meta: { title: 'Tree', icon: 'tree', access: ['Tree01'] }
      }
    ]
  },

  {
    path: '/setting',
    component: Layout,
    redirect: '/setting/roles',
    name: 'setting',
    meta: { title: '系统设置', icon: 'el-icon-s-help', access: ['setting'] },
    children: [
      {
        path: 'roles',
        name: 'roles',
        component: () => import('@/views/setting/roles/index'),
        meta: { title: '角色设置', icon: 'table', access: ['roles'] }
      },
      {
        path: 'users',
        name: 'users',
        component: () => import('@/views/setting/users/index'),
        meta: { title: '用户设置', icon: 'tree', access: ['users'] }
      }
    ]
  },

  {
    path: '/book',
    component: Layout,
    redirect: '/book/article',
    name: 'book',
    meta: { title: '文章设置', icon: 'el-icon-s-help', access: ['book'] },
    children: [
      {
        path: 'article',
        name: 'article',
        component: () => import('@/views/book/article/index'),
        meta: { title: '内容', icon: 'table' }
      },
      {
        path: 'tree01',
        name: 'Tree01',
        component: () => import('@/views/tree/index'),
        meta: { title: 'Tree', icon: 'tree' }
      }
    ]
  },

  {
    path: '/404',
    name: 'error_404',
    component: () => import('@/views/error/404'),
    hidden: true
  },

  {
    path: '/401',
    name: 'error_401',
    component: () => import('@/views/error/401'),
    hidden: true
  },

  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]

export default routes
