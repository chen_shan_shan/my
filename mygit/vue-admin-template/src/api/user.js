import request from '@/utils/request'

export function login(data) {
  return request({
    url: '/api/users/login',
    method: 'post',
    data
  })
}

export function getInfo(token) {
  return request({
    url: '/api/users/getUserInfo',
    method: 'get',
    params: { token }
  })
}

export function logout() {
  return request({
    url: '/api/users/logout',
    method: 'post'
  })
}

// 用户列表
export function getUsers(pIndex, pSize) {
  return request({
    url: `/api/users?pageIndex=${pIndex}&pageSize=${pSize}`,
    method: 'get'
  })
}

// 用户添加
export function addUsers(data) {
  return request({
    url: '/api/users',
    method: 'post',
    data
  })
}

// 用户更新
export function updateUsers(id, data) {
  return request({
    url: '/api/users/' + id,
    method: 'put',
    data
  })
}

// 用户删除
export function deleteUsers(id) {
  return request({
    url: '/api/users/' + id,
    method: 'delete'
  })
}

// 用户角色修改
export function updateUserRoles(userId, data) {
  return request({
    url: `/api/users/${userId}/updateUserRole`,
    method: 'post',
    data
  })
}

// 用户角色状态修改
export function updateUserStatus(userId) {
  return request({
    url: `/api/users/${userId}/status`,
    method: 'post'
  })
}
