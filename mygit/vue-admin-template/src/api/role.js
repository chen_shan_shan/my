import request from '@/utils/request'

export function getRoles() {
  return request({
    url: '/api/roles',
    method: 'get'
  })
}

// 角色添加
export function addRoles(data) {
  return request({
    url: '/api/roles',
    method: 'post',
    data
  })
}

// 角色更新
export function updateRoles(id, data) {
  return request({
    url: '/api/roles/' + id,
    method: 'put',
    data
  })
}

// 角色删除
export function deleteRoles(id) {
  return request({
    url: '/api/roles/' + id,
    method: 'delete'
  })
}

export function saveRolePermission(id, data) {
  return request({
    url: `/api/roles/${id}/permission`,
    method: 'post',
    data
  })
}
