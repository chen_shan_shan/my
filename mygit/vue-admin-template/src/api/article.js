import request from '@/utils/request'

export function getArticle() {
  return request({
    url: '/api/article',
    method: 'get'
  })
}

// 添加文章
export function addArticles(data) {
  return request({
    url: '/api/articles',
    method: 'post',
    data
  })
}

// 文章更新
export function updateArticles(id, data) {
  return request({
    url: '/api/articles/' + id,
    method: 'put',
    data
  })
}

// 文章删除
export function deleteArticles(id) {
  return request({
    url: '/api/articles/' + id,
    method: 'delete'
  })
}

export function saveArticlePermission(id, data) {
  return request({
    url: `/api/articles/${id}/permission`,
    method: 'post',
    data
  })
}
