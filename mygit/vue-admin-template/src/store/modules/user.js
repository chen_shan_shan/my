import { login, logout, getInfo } from '@/api/user'
import { getToken, setToken, removeToken } from '@/utils/auth'
import { resetRouter } from '@/router'

var user = {
  namespaced: true,
  state: {
    token: '',
    name: '',
    avatar: '',
    access: []
  },
  mutations: {
    resetState: (state) => {
      var tempState = {
        token: getToken(),
        name: '',
        avatar: '',
        access: []
      }
      Object.assign(state, tempState)
    },
    setToken: (state, token) => {
      state.token = token
    },
    setName: (state, name) => {
      state.name = name
    },
    setAvatar: (state, avatar) => {
      state.avatar = avatar
    },
    setAccess: (state, access) => {
      state.access = access
    }
  },
  actions: {
    // user login
    login({ commit }, parms) {
      var userInfo = parms.loginform
      var remeberMe = parms.remberMe
      // const { username, password } = userInfo
      return new Promise((resolve, reject) => {
        // var user = { username: username.trim(), password: password }
        login(userInfo).then(response => {
          console.log(response)
          const { data } = response
          commit('setToken', data.token)
          // commit('setAccess', data.access)
          setToken(data.token, remeberMe)
          resolve()
        }).catch(error => {
          reject(error)
        })
      })
    },

    // get user info
    getInfo({ commit, state }) {
      return new Promise((resolve, reject) => {
        var token = ''
        if (state.token) {
          token = state.token
        } else {
          token = getToken()
        }
        getInfo(token).then(response => {
          console.log(response)
          const { data } = response
          if (!data) {
            return reject('验证失败，请重新登录！')
          }

          const { name, avatar, access } = data
          var avatarUrl = process.env.VUE_APP_BASE_API + avatar
          commit('setName', name)
          commit('setAvatar', avatarUrl)
          commit('setAccess', access)
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // user logout
    logout({ commit, state }) {
      return new Promise((resolve, reject) => {
        logout(state.token).then(() => {
          removeToken() // must remove  token  first
          resetRouter()
          commit('resetState')
          resolve()
        }).catch(error => {
          reject(error)
        })
      })
    },

    // remove token
    resetToken({ commit }) {
      return new Promise(resolve => {
        removeToken() // must remove  token  first
        commit('resetState')
        resolve()
      })
    }
  }
}

export default user

