// import Cookies from 'js-cookie'

var sidebarKey = 'sidebarStatus'

var getSidebarStatus = () => {
  return localStorage.getItem(sidebarKey)
}

var setSidebarStatus = (status) => [
  localStorage.setItem(sidebarKey, status)
]

var app = {
  namespaced: true,
  state: {
    sidebar: {
      opened: getSidebarStatus() ? !!+getSidebarStatus() : true,
      withoutAnimation: false
    },
    device: 'desktop'
  },
  mutations: {
    toggleSideBar: state => {
      state.sidebar.opened = !state.sidebar.opened
      state.sidebar.withoutAnimation = false
      if (state.sidebar.opened) {
        setSidebarStatus(1)
        // Cookies.set('sidebarStatus', 1)
      } else {
        setSidebarStatus(0)
        // Cookies.set('sidebarStatus', 0)
      }
    },
    closeSideBar: (state, withoutAnimation) => {
      // Cookies.set('sidebarStatus', 0)
      setSidebarStatus(0)
      state.sidebar.opened = false
      state.sidebar.withoutAnimation = withoutAnimation
    },
    toggleDevice: (state, device) => {
      state.device = device
    }
  },
  actions: {
    toggleSideBar({ commit }) {
      commit('toggleSideBar')
    },
    closeSideBar({ commit }, { withoutAnimation }) {
      commit('closeSideBar', withoutAnimation)
    },
    toggleDevice({ commit }, device) {
      commit('toggleDevice', device)
    }
  }
}

export default app
